package edu.buffalo.cse.pocketattacker;

import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class WifiSSLoggerActivity extends Activity {
	// debugging
	private static final String TAG = "WifiSSLoger";
	private static final boolean D = true;
	
    // Layout Views
    private Button mLogButton; // log button
    private Button mScanButton; // scan button
    private EditText mInputDistance; // distance from an AP
    private TextView mSSID;
    private TextView mBSSID;
    private TextView mSignalStrength;
	
    private StringBuffer mOutSSIDBuffer = new StringBuffer(); // SSID
    private StringBuffer mOutBSSIDBuffer = new StringBuffer(); // MAC address
    private StringBuffer mOutSSBuffer = new StringBuffer(); // signal strength
    
    private BroadcastReceiver receiver = null;
    
    @Override
    public void onDestroy() {
    	unregisterReceiver(receiver);
    }
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mLogButton = (Button) findViewById(R.id.button_log);
        mScanButton = (Button) findViewById(R.id.button_scan);
        mInputDistance = (EditText) findViewById(R.id.edit_text_distance);
        mSSID = (TextView) findViewById(R.id.tv_ssid);
        mBSSID = (TextView) findViewById(R.id.tv_bssid);
        mSignalStrength = (TextView) findViewById(R.id.tv_signal);
        
        IntentFilter i = new IntentFilter(); 
        i.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        receiver = new BroadcastReceiver() { 
	        public void onReceive(Context c, Intent i) { 
	        	// Code to execute when SCAN_RESULTS_AVAILABLE_ACTION event occurs 
	        	WifiManager w = (WifiManager) c.getSystemService(Context.WIFI_SERVICE); 
	        	List<ScanResult> l = w.getScanResults(); // Returns a <list> of scanResults 
	        	
	        	
	        	Iterator<ScanResult> it = l.iterator();
	        	while (it.hasNext()) {
	        		ScanResult result = it.next();
	        		Log.d(TAG,  
	        				" SSID: " + result.SSID + " MAC: " + result.BSSID + 
	        				" Signal: " + result.level);
	        	}
	        	
	        	Toast.makeText(getApplicationContext(), "AP ScanResult delivered.", Toast.LENGTH_SHORT);
	        } };
	    
        registerReceiver(receiver, i ); 
        
        mScanButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	WifiManager wifiMgr = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                boolean ret;
                
                if (!wifiMgr.isWifiEnabled()) {
                	Log.d(TAG, "WiFi is not enabled. Turning it on...");
                	wifiMgr.setWifiEnabled(true);
                	Toast.makeText(getApplicationContext(), "WiFi is not enabled. Turning it on...", Toast.LENGTH_SHORT);
                }
            	
                ret = wifiMgr.startScan();
                if (ret == false) {
                	Log.d(TAG, "Something went wrong with getSystemService(Context.WIFI_SERVICE).");
                	Toast.makeText(getApplicationContext(), "Something wrong with getSystemService(Context.WIFI_SERVICE).", Toast.LENGTH_SHORT);
                }
            }
        });
        
    }
    
    
}